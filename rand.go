// package rand serves to give some nice opinionated defaults on how to create PRNGs.
// NewDefault() should suffice for most needs.
package rand

import (
	crand "crypto/rand"
	"fmt"
	"math"
	"math/big"
	"math/rand"
	"sync"
)

// NewDefault gives you a concurrency-safe default RNG, initialized via the OS's source of randomness.
// While it is _initialized_ in a cryptographically-secure, os-dependent way, THIS DOES NOT MEAN the rng itself is cryptographically secure;
// it uses the same underlying PRNG logic as in the math/rand stdlib.
func NewDefault() *rand.Rand { return New(NewLockedSource(OSRandSeed())) }

// New creates a new rng from the given source. It's just an alias for math/rand.New
func New(source rand.Source) *rand.Rand { return rand.New(source) }

// NewLockedSource creates a concurrency-safe random source. This is slower than NewLocalSource, but safer. Default to this.
func NewLockedSource(seed int64) rand.Source { return &lockedSrc{src: rand.NewSource(seed)} }

// NewLocalSource creates a non-concurrency-safe random source. It's just an alias for math/rand.NewSource.
// This is faster than NewLockedSource, but not concurrency-safe. Default to that one.
func NewLocalSource(seed int64) rand.Source { return rand.NewSource(seed) }

// OsRandSeed creates a random uint64 by calling the operating system's source of randomness (crypto/rand.Reader).
// See the docs at https://golang.org/pkg/crypto/rand/#pkg-variables for more info.
func OSRandSeed() int64 {
	seed, err := crand.Int(crand.Reader, big.NewInt(math.MaxInt64))
	if err != nil {
		panic(fmt.Errorf("could not reach the OS's source of randomness: %v", err))
	}
	return seed.Int64()
}

type lockedSrc struct {
	src rand.Source
	mux sync.Mutex
}

func (s *lockedSrc) Seed(seed int64) {
	s.mux.Lock()
	s.src.Seed(seed)
	s.mux.Unlock()
}

func (s *lockedSrc) Int63() int64 {
	s.mux.Lock()
	defer s.mux.Unlock()
	return s.src.Int63()
}

func (s *lockedSrc) Uint64() uint64 {
	s.mux.Lock()
	defer s.mux.Unlock()
	if s, ok := s.src.(rand.Source64); ok {
		return s.Uint64()
	}
	low := uint64(s.src.Int63()) >> 31
	high := uint64(s.src.Int63()) << 32
	return low | high

}
