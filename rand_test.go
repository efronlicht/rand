package rand_test

import (
	"sync"
	"testing"

	"gitlab.com/efronlicht/rand"
)

//go:noinline
func Test_Locked(t *testing.T) {
	wg := new(sync.WaitGroup)
	const procs = 200
	wg.Add(procs)
	rng := rand.NewDefault()
	// try and set off the race detector by asking for randomness via a bunch of goroutines
	for i := 0; i < procs; i++ {
		go func() {
			defer wg.Done()
			rng.Int63()
		}()
	}
	wg.Wait()
}

func Test_ApproxDistribution(t *testing.T) {
	const n = 1_000_000
	if testing.Short() {
		t.Skipf("takes a while to run %d iterations", n)
	}
	var buf [256]float64
	rng := rand.New(rand.NewLocalSource(rand.OSRandSeed()))
	for i := 0; i < n; i++ {
		buf[rng.Intn(256)]++
	}
	// by the law of large numbers, if this is actually approximately random, we won't hit a large outlier
	for i, x := range buf {
		const max = n * 2.0 / 256.0
		const min = n / 256.0 / 2.0
		if x < min || x > max {
			t.Errorf("expected the proporition of %d to be between %v and %v, but got %v", i, min, max, x)
		}

	}
}
